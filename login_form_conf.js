(function($) {
 Drupal.behaviors.checkKeyPress = {
  attach: function() {
   jQuery('.form-title').on('keyup', function() {
    jQuery('.form-title-preview').html(jQuery('.form-title').val());
   });
   jQuery('.username-label').on('keyup', function() {
    jQuery('.username-label-preview').html(jQuery('.username-label').val());
   });
   jQuery('.username-description').on('keyup', function() {
    jQuery('.username-description-preview').html(jQuery('.username-description').val());
   });
   jQuery('.password-label').on('keyup', function() {
    jQuery('.password-label-preview').html(jQuery('.password-label').val());
   });
   jQuery('.password-description').on('keyup', function() {
    jQuery('.password-description-preview').html(jQuery('.password-description').val());
   });
   jQuery('.login-button').on('keyup', function() {
    jQuery('.login-button-preview').html(jQuery('.login-button').val());
   });
   
   jQuery("a.switch-default").on("click", function (e) {
        var link = this;

        e.preventDefault();

        jQuery("<div>Are you sure you want to reset the form values to default?</div>").dialog({
		resizable: false,
      height: "auto",
		modal:true,
    width: 400,
			 buttons: {
                "Ok": function () {
                    window.location = link.href;
                },
                "Cancel": function () {
                    $(this).dialog("close");
                }
            },
        });

    });
   
   if (jQuery('.form-item-create-account input[type="checkbox"]').prop("checked") == true) {
   jQuery('.create-account-preview').show();
   }
   else {
   jQuery('.create-account-preview').hide();
   }
   
   if (jQuery('.form-item-forgot-password input[type="checkbox"]').prop("checked") == true) {
   jQuery('.forgot-password-preview').show();
   jQuery('.create-account-preview a').html('Create New Account');
   }
   else {
   jQuery('.forgot-password-preview').hide();
   jQuery('.create-account-preview a').html('Create New Account</br></br>');
   }
   
   if((jQuery('.form-item-create-account input[type="checkbox"]').prop("checked") == true) && (jQuery('.form-item-forgot-password input[type="checkbox"]').prop("checked") == true)) {
	 jQuery('.dummy-pipe').show();
   }
   else {
   jQuery('.dummy-pipe').hide();
   }
   
   jQuery('.form-item-create-account input[type="checkbox"]').click(function () {
   show_pipe();
            if (jQuery(this).prop("checked") == true) {
				jQuery('.create-account-preview').show();
				
            }
             else if (jQuery(this).prop("checked") == false) {
                jQuery('.create-account-preview').hide();
				
            }
        });
		
		jQuery('.form-item-forgot-password input[type="checkbox"]').click(function () {
		show_pipe();
            if (jQuery(this).prop("checked") == true) {
				jQuery('.forgot-password-preview').show();
				jQuery('.create-account-preview a').html('Create New Account');
				
            }
             else if (jQuery(this).prop("checked") == false) {
                jQuery('.forgot-password-preview').hide();
				jQuery('.create-account-preview a').html('Create New Account</br></br>');
            }
        });
   function show_pipe() {
   if((jQuery('.form-item-create-account input[type="checkbox"]').prop("checked") == true) && (jQuery('.form-item-forgot-password input[type="checkbox"]').prop("checked") == true)) {
	 jQuery('.dummy-pipe').show();
   }
   else {
   jQuery('.dummy-pipe').hide();
   }
   }
   
   
  } 
  
 }
})(jQuery);