<?php

/**
 * @file
 * This module is for Login Configuration.
 */

/**
 * Implements hook_menu().
 */
function login_form_conf_menu() {
  $items['admin/config/user-interface/login-conf'] = array(
    'title' => 'Login Form Configure',
    'description' => 'Configuring the login form',
    'page callback' => 'login_config_form',
    'access arguments' => array('administer login configuration'),
  );
  $items['switch_to_default'] = array(
    'title' => 'Clearing the dates',
    'page callback' => 'switch_to_default_form',
    'access arguments' => array('administer login configuration'),
  );
  return $items;
}

/**
 * Default form callback.
 *
 * @return array
 *   Text something.
 */
function switch_to_default_form() {
  drupal_set_message(t('The configurations are cleared. Default login form set.'));
  variable_del('login_form_config');
  drupal_goto("admin/config/user-interface/login-conf");

  return "something";
}

/**
 * The configuraton form callback for login form.
 *
 * @return array
 *   Returns the login configuration fields form and the dummy login form.
 */
function login_config_form() {
  drupal_add_js(drupal_get_path('module', 'login_form_conf') . '/login_form_conf.js', 'file');
  $output = array();

  $form['preview_form'] = array(
    '#type' => 'markup',
    '#markup' => '<h1>
            Preview        </h1>',
  );

  $output[0] = drupal_get_form('login_config_fields');
  $output[1] = $form;
  $output[2] = drupal_get_form('login_dummy');
  return $output;
}

/**
 * The configuraton form for changing labels and description.
 *
 * @return array
 *   Returns the login configuration form.
 */
function login_config_fields($form, &$form_state) {
  $login_form_config = variable_get('login_form_config');

  $form['form_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Login Form Title'),
    '#size' => 60,
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#attributes' => array('class' => array('form-title')),
    '#required' => TRUE,
    '#default_value' => !empty($login_form_config['form_title']) ? $login_form_config['form_title'] : 'User account',
  );
  $form['username_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Username Label'),
    '#size' => 60,
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#attributes' => array('class' => array('username-label')),
    '#required' => TRUE,
    '#default_value' => !empty($login_form_config['username_label']) ? $login_form_config['username_label'] : 'Username',
  );
  $form['username_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Username Description'),
    '#size' => 60,
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#attributes' => array('class' => array('username-description')),
    '#required' => TRUE,
    '#default_value' => !empty($login_form_config['username_description']) ? $login_form_config['username_description'] : t('Enter your @s username.', array('@s' => variable_get('site_name', 'Drupal'))),
  );
  $form['password_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Password Label'),
    '#size' => 60,
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#attributes' => array('class' => array('password-label')),
    '#required' => TRUE,
    '#default_value' => !empty($login_form_config['password_label']) ? $login_form_config['password_label'] : 'Password',
  );
  $form['password_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Password Description'),
    '#size' => 60,
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#attributes' => array('class' => array('password-description')),
    '#required' => TRUE,
    '#default_value' => !empty($login_form_config['password_description']) ? $login_form_config['password_description'] : 'Enter the password that accompanies your username.',
  );

  $form['login_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Login button text'),
    '#size' => 60,
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#attributes' => array('class' => array('login-button')),
    '#default_value' => !empty($login_form_config['login_button']) ? $login_form_config['login_button'] : 'Log in',
  );

  $form['create_account'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($login_form_config['create_account']) ? $login_form_config['create_account'] : 0,
    '#title' => t('Create New Account'),
  );
  $form['forgot_password'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($login_form_config['forgot_password']) ? $login_form_config['forgot_password'] : 0,
    '#title' => t('Forgot your password?'),
  );
  $form['login_block'] = array(
    '#type' => 'checkbox',
    '#default_value' => !empty($login_form_config['login_block']) ? $login_form_config['login_block'] : 0,
    '#title' => t('Configure the login block as well?'),
  );
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'login_form_conf') . '/login_form_conf.css',
  );

  $form['#attached']['js'] = array(
    'https://code.jquery.com/ui/1.12.1/jquery-ui.js',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
  );

  $form['default_form'] = array(
    '#markup' => '<a href="/switch_to_default" class="switch-default" >Switch to Default</a>',
  );

  return $form;
}

/**
 * Implementation of form_submit function.
 */
function login_config_fields_submit($form, &$form_state) {
  variable_del('login_form_config');
  $form_title = $form_state['values']['form_title'];
  $username_label = $form_state['values']['username_label'];
  $username_description = $form_state['values']['username_description'];
  $password_label = $form_state['values']['password_label'];
  $password_description = $form_state['values']['password_description'];
  $login_button = $form_state['values']['login_button'];
  $create_account = $form_state['values']['create_account'];
  $forgot_password = $form_state['values']['forgot_password'];
  $login_block = $form_state['values']['login_block'];
  $array_value = array(
    'form_title' => $form_title,
    'username_label' => $username_label,
    'username_description' => $username_description,
    'password_label' => $password_label,
    'password_description' => $password_description,
    'login_button' => $login_button,
    'create_account' => $create_account,
    'forgot_password' => $forgot_password,
    'login_block' => $login_block,
  );
  // Assigning the values to the variable.
  variable_set('login_form_config', $array_value);
  drupal_set_message(t('Your Configurations are saved.'));
}

/**
 * The dummy form which replicates the default login form.
 *
 * @return array
 *   Returns the dummy form fields.
 */
function login_dummy($form, &$form_state) {

  $login_form_config = variable_get('login_form_config');

  $form_title_preview = !empty($login_form_config['form_title']) ? $login_form_config['form_title'] : 'User account';
  $username_label_preview = !empty($login_form_config['username_label']) ? $login_form_config['username_label'] : 'Username';
  $username_description_preview = !empty($login_form_config['username_description']) ? $login_form_config['username_description'] : t('Enter your @s username.', array('@s' => variable_get('site_name', 'Drupal')));
  $password_label_preview = !empty($login_form_config['password_label']) ? $login_form_config['password_label'] : 'Password';
  $password_description_preview = !empty($login_form_config['password_description']) ? $login_form_config['password_description'] : 'Enter the password that accompanies your username.';
  $login_button_preview = !empty($login_form_config['login_button']) ? $login_form_config['login_button'] : 'Log in';

  $form['form_title_preview'] = array(
    '#type' => 'markup',
    '#markup' => '<h1 class="title"><span class="form-title-preview">' . $form_title_preview . '</span></h1>',
  );

  $form['dummy_name'] = array(
    '#type' => 'textfield',
    '#title' => t('<span class="username-label-preview">@username-label-preview</span>', array('@username-label-preview' => $username_label_preview)),
    '#size' => 60,
    '#maxlength' => USERNAME_MAX_LENGTH,
    '#required' => TRUE,
  );
  $form['dummy_name']['#description'] = t('<span class="username-description-preview">@username-description-preview</span>', array('@username-description-preview' => $username_description_preview));
  $form['dummy_pass'] = array(
    '#type' => 'password',
    '#title' => t('<span class="password-label-preview">@password-label-preview</span>', array('@password-label-preview' => $password_label_preview)),
    '#description' => t('<span class="password-description-preview">@password-description-preview</span>', array('@password-description-preview' => $password_description_preview)),
    '#required' => TRUE,
  );

  $form['create_account'] = array(
    '#markup' => '<span class="create-account-preview"><a href="/user/register" >Create New Account</a> </span>',
  );
  $form['dummy_pipe'] = array(
    '#markup' => '<span class="dummy-pipe"> | </span>',
  );
  $form['forgot_password'] = array(
    '#markup' => '<span class="forgot-password-preview"><a href="/user/password" >Forgot your password?</a></br></br></span>',
  );

  $form['submit'] = array(
    '#type' => 'markup',
    '#markup' => '<span class="login-button-preview">' . $login_button_preview . '</span>',
    '#attributes' => array('class' => array('form-submit')),
  );

  return $form;
}

/**
 * Implements hook_form_alter().
 */
function login_form_conf_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == "user_login_block") {
    $login_form_config = variable_get('login_form_config');
    if ($login_form_config['login_block'] == 1) {

      $form['name']['#title'] = !empty($login_form_config['username_label']) ? $login_form_config['username_label'] : 'Username';
      $form['name']['#description'] = !empty($login_form_config['username_description']) ? $login_form_config['username_description'] : t('Enter your @s username.', array('@s' => variable_get('site_name', 'Drupal')));
      $form['pass']['#title'] = !empty($login_form_config['password_label']) ? $login_form_config['password_label'] : 'Password';
      $form['pass']['#description'] = !empty($login_form_config['password_description']) ? $login_form_config['password_description'] : 'Enter the password that accompanies your username.';

      $form['actions']['submit']['#value'] = !empty($login_form_config['login_button']) ? $login_form_config['login_button'] : 'Log in';
    }
  }

  if ($form_id == 'user_login') {
    $login_form_config = variable_get('login_form_config');

    if (!empty($login_form_config['form_title'])) {
      drupal_add_js("jQuery(document).ready(function () {
  jQuery('.page-user-login .tabs').hide(); });", "inline");
      drupal_add_js("jQuery(document).ready(function () {
  jQuery('.page-user.not-logged-in .tabs').hide(); });", "inline");

      drupal_set_title($login_form_config['form_title']);
    }

    $form['name']['#title'] = !empty($login_form_config['username_label']) ? $login_form_config['username_label'] : 'Username';
    $form['name']['#description'] = !empty($login_form_config['username_description']) ? $login_form_config['username_description'] : t('Enter your @s username.', array('@s' => variable_get('site_name', 'Drupal')));
    $form['pass']['#title'] = !empty($login_form_config['password_label']) ? $login_form_config['password_label'] : 'Password';
    $form['pass']['#description'] = !empty($login_form_config['password_description']) ? $login_form_config['password_description'] : 'Enter the password that accompanies your username.';
    if ($login_form_config['create_account'] == 1) {
      $form['create_account'] = array(
        '#markup' => '<a href="/user/register" class="switch-default" >Create New Account </a>',
      );

    }
    if (($login_form_config['create_account'] == 1) && ($login_form_config['forgot_password'] == 1)) {
      $form['pipe'] = array(
        '#markup' => '<span class="pipe"> | </span>',
      );
    }

    if ($login_form_config['forgot_password'] == 1) {
      $form['forgot_password'] = array(
        '#markup' => '<a href="/user/password" class="switch-default" >Forgot your password?</a></br>',
      );
    }
    $form['actions']['submit']['#value'] = !empty($login_form_config['login_button']) ? $login_form_config['login_button'] : 'Log in';
  }
}

/**
 * Implements hook_permission().
 */
function login_form_conf_permission() {
  return array(
    'administer login configuration' => array(
      'title' => t('Administer Login Configuration'),
      'description' => t('Administration page for Login Form Configuration.'),
    ),
  );
}
