=====
Login Form Configure
https://www.drupal.org/project/login_form_conf
-----
Login Form Configure has been developed and maintained by Danish.
https://www.drupal.org/u/danisha
=====
Installation
-----

1. Enable the module and its dependent modules.
2. Clear the cache and go to the configuration page at 
admin/config/user-interface/login-conf
3. Check for the permissions named "Administer Login Configuration",
if assigned to the role who needs to view the page.
3. You can see the preview of the default login form.
You can alter the form labels, descriptions and button text. 
Also you can use the same configuration for the login block. 
Save the configuration and go to user/login by anonymous user.
4. This module works only on the default login form configuration. 
If the login form is being altered in any way then it might
conflict with this module.
